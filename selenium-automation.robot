***settings***
library     String
library     SeleniumLibrary

***variables***
${browser}      chrome
${homepage}     automationpractice.com/index.php
${scheme}       http
${prodScheme}   https
${testUrl}      ${scheme}://${homepage}
${prodUrl}      ${prodScheme}://${homepage}
@{nombresDeContenedores}    //*[@id="homefeatured"]/li[1]/div/div[2]/h5/a   //*[@id="homefeatured"]/li[2]/div/div[2]/h5/a   //*[@id="homefeatured"]/li[3]/div/div[2]/h5/a   //*[@id="homefeatured"]/li[4]/div/div[2]/h5/a   //*[@id="homefeatured"]/li[5]/div/div[2]/h5/a   //*[@id="homefeatured"]/li[6]/div/div[2]/h5/a   //*[@id="homefeatured"]/li[7]/div/div[2]/h5/a



***keywords***
Open Homepage
    Open Browser    ${testUrl}      ${browser}

***test cases***
C001 Hacer Click en Contenedores
    Open Homepage
    :FOR     ${nombreDeContenedor}   IN      @{nombresDeContenedores}
    \   Click Element   xpath=${nombreDeContenedor}
    \   Wait Until Element Is Visible   xpath=//*[@id="bigpic"]
    \   Click Element   xpath=//*[@id="header_logo"]/a/img
    Close Browser

C002 Caso de Prueba Nuevo
    Open Homepage